package me.timos.thuanle.base.application;

import android.app.Application;
import android.support.v4.app.DialogFragment;

import me.timos.thuanle.base.activity.IActivityFactory;
import me.timos.thuanle.base.dialog.IDialogFactory;
import me.timos.thuanle.base.fragment.IFragmentFactory;
import me.timos.thuanle.base.fragment.NavigableScreen;

public abstract class BaseApplication extends Application implements IFragmentFactory, IActivityFactory, IDialogFactory {
    protected static BaseApplication _INSTANCE;

    IFragmentFactory mFragmentFactory;
    IActivityFactory mActivityFactory;
    IDialogFactory mDialogFactory;

    public static BaseApplication getInstance() {
        return _INSTANCE;
    }

    public DialogFragment createDialog(String dialogTag) {
        return mDialogFactory.createDialog(dialogTag);
    }

    @Override
    public NavigableScreen createFragment(String fragmentTag) {
        return mFragmentFactory.createFragment(fragmentTag);
    }

    @Override
    public Class<?> getActivityClass(String tag) {
        return mActivityFactory.getActivityClass(tag);
    }

    protected abstract IActivityFactory getActivityFactory();

    protected abstract IDialogFactory getDialogFactory();

    protected abstract IFragmentFactory getFragmentFactory();

    @Override
    public void onCreate() {
        super.onCreate();

        _INSTANCE = this;
        mActivityFactory = getActivityFactory();
        mFragmentFactory = getFragmentFactory();
        mDialogFactory = getDialogFactory();
    }
}
