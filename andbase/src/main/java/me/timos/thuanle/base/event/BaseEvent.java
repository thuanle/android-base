package me.timos.thuanle.base.event;

import org.greenrobot.eventbus.EventBus;

public abstract class BaseEvent {
    public void post() {
        EventBus.getDefault().post(this);
    }
}
