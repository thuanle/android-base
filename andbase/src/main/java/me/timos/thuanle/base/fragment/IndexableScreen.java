package me.timos.thuanle.base.fragment;

import android.support.annotation.DrawableRes;

/**
 * Created by thuanle on 2/24/17.
 */

public interface IndexableScreen {
    @DrawableRes
    int getIconResId();

    String getKeywords();

    String getScreenTag();

    String getTitle();
}
