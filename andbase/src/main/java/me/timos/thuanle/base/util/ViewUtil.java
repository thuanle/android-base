package me.timos.thuanle.base.util;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.LinearLayout;

public class ViewUtil {
    public static Point getCenter(View view) {
        return new Point((view.getLeft() + view.getRight()) / 2, (view.getTop() + view.getBottom()) / 2);
    }

    public static Point getCenterWithOffsetParent(View view) {
        View parent = (View) view.getParent();
        Point center = getCenter(view);
        center.offset(parent.getLeft(), parent.getTop());
        return center;
    }

    public static void hideKeyboard(Activity context) {
        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        // check if no view has focus:
        View view = context.getCurrentFocus();
        if (view != null) {
            inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void setLayoutWeight(View view, int weight) {
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) view.getLayoutParams();
        lp.weight = weight;
        view.setLayoutParams(lp);
    }
}
