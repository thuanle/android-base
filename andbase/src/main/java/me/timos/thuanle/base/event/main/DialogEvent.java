package me.timos.thuanle.base.event.main;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import me.timos.thuanle.base.event.BaseEvent;

public class DialogEvent extends BaseEvent {
    public final DialogEvent.Param mParam;

    private DialogEvent(DialogEvent.Param param) {
        mParam = param;
    }

    public static class Param {
        public String dialogTag;
        public Bundle data;
    }

    public static class Builder {
        DialogEvent.Param mParam;

        private Builder() {
            mParam = new DialogEvent.Param();
            mParam.data = new Bundle();
        }

        public static DialogEvent.Builder custom(@NonNull String dialogTag) {
            DialogEvent.Builder builder = new DialogEvent.Builder();
            builder.mParam.dialogTag = dialogTag;
            return builder;
        }

        public DialogEvent.Builder addArg(@NonNull String key, @NonNull String value) {
            mParam.data.putString(key, value);
            return this;
        }

        public DialogEvent.Builder addArgInt(@NonNull String key, int value) {
            mParam.data.putInt(key, value);
            return this;
        }

        public DialogEvent.Builder addArgList(@NonNull String key, @NonNull ArrayList<String> value) {
            mParam.data.putStringArrayList(key, value);
            return this;
        }

        public DialogEvent build() {
            return new DialogEvent(mParam);
        }
    }

}
