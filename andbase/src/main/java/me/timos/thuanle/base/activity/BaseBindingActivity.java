package me.timos.thuanle.base.activity;

import android.support.annotation.ColorInt;
import android.support.annotation.IdRes;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;

public abstract class BaseBindingActivity extends BaseActivity {

    protected void goneWhen(@IdRes int resId, boolean condition) {
        View v = findViewById(resId);
        v.setVisibility(condition ? View.GONE : View.VISIBLE);
    }

    protected void imageView(@IdRes int resId, String url) {
        ImageView iv = ButterKnife.findById(this, resId);
        Picasso.with(this).load(url).into(iv);
    }

    protected void invisibleWhen(@IdRes int resId, boolean condition) {
        View v = findViewById(resId);
        v.setVisibility(condition ? View.INVISIBLE : View.VISIBLE);
    }

    /**
     * Set text color for a specific TextView
     *
     * @param resId    the key of the TextView
     * @param colorInt the color int
     */
    protected void textColor(@IdRes int resId, @ColorInt int colorInt) {
        TextView tv = ButterKnife.findById(this, resId);
        tv.setTextColor(colorInt);
    }

    /**
     * Set a particular text to a specific textview
     *
     * @param resId the key of the TextView
     * @param text  the text
     */
    protected void textView(@IdRes int resId, CharSequence text) {
        TextView tv = ButterKnife.findById(this, resId);
        tv.setText(text);
    }

}
