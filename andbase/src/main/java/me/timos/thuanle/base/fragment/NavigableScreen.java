package me.timos.thuanle.base.fragment;

public interface NavigableScreen {
    String getTitle();
}
