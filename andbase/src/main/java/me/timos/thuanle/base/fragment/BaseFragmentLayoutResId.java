package me.timos.thuanle.base.fragment;

import android.view.View;

/**
 * Base Fragment with layout inflated from a xml resource.
 */
public abstract class BaseFragmentLayoutResId extends BaseBindingFragment {
    @Deprecated
    @Override
    final public View getContentView() {
        return null;
    }
}
