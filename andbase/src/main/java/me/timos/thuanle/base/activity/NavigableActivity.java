package me.timos.thuanle.base.activity;

import android.os.Bundle;

public interface NavigableActivity {
    void showScreen(String screenTag, Bundle data);

    void showScreen(String screenTag, Bundle data, boolean requestRefresh);

    void showScreen(String screenTag, String activityTag, Bundle data, boolean isBackable);
}
