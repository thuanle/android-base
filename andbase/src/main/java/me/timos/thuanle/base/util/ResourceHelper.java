package me.timos.thuanle.base.util;

import android.content.ContentResolver;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.ColorRes;
import android.support.annotation.DimenRes;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.RawRes;
import android.support.annotation.StringRes;
import android.util.Base64;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.TextView;

import java.io.File;
import java.io.InputStream;

import me.timos.thuanle.base.application.BaseApplication;

/**
 * Helper for resource work.
 */
public class ResourceHelper {

    public static void countDownAnimation(final TextView textView, final int countInSeconds, final CountDownAnimationCallback countDownAnimationCallback) {
        if (countInSeconds == 0) {
            TextViewUtil.clearText(textView);
            countDownAnimationCallback.onCountDownEnd();
            return;
        }

        textView.setText(String.valueOf(countInSeconds));
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(1000); // 1 sec
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationEnd(Animation animation) {
                countDownAnimation(textView, countInSeconds - 1, countDownAnimationCallback);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }

            @Override
            public void onAnimationStart(Animation animation) {

            }
        });

        textView.startAnimation(alphaAnimation);
    }

    @NonNull
    public static String getBase64String(@StringRes int resId) throws java.io.IOException {
        String base64 = BaseApplication.getInstance().getString(resId);
        byte[] data = Base64.decode(base64, Base64.DEFAULT);
        return new String(data, "UTF-8");
    }

    public static File getCacheDir() {
        return BaseApplication.getInstance().getCacheDir();
    }

    @NonNull
    public static int getColor(@ColorRes int resId) {
        if (Build.VERSION.SDK_INT >= 23) {
            return BaseApplication.getInstance().getColor(resId);
        } else {
            return getResources().getColor(resId);
        }
    }

    public static String getColorString(int resId) {
        return "#" + getString(resId).substring(3);
    }

    public static File getDBPath(String dbName) {
        return BaseApplication.getInstance().getDatabasePath(dbName);
    }

    public static float getDimension(@DimenRes int dimenId) {
        return getResources().getDimension(dimenId);
    }

    private static Resources getResources() {
        return BaseApplication.getInstance().getResources();
    }

    @NonNull
    public static String getString(@StringRes int resId) {
        return BaseApplication.getInstance().getString(resId);
    }

    @NonNull
    public static CharSequence getString(@StringRes int resId, Object... formatArgs) {
        return BaseApplication.getInstance().getString(resId, formatArgs);
    }

    public static Uri getUriToDrawable(@DrawableRes int drawableId) {
        Uri imageUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE +
                "://" + getResources().getResourcePackageName(drawableId)
                + '/' + getResources().getResourceTypeName(drawableId)
                + '/' + getResources().getResourceEntryName(drawableId));
        return imageUri;
    }

    public static boolean moveFile(String fromPath, String toPath) {
        File from = new File(fromPath);
        File to = new File(toPath);
        return from.renameTo(to);
    }

    public static InputStream openRawResource(@RawRes int rawId) {
        return getResources().openRawResource(rawId);
    }

    public static boolean removeTemp(String filePath) {
        File file = new File(filePath);
        return file.delete();
    }

    public static Uri resourceToUri(int resID) {
        return Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" +
                getResources().getResourcePackageName(resID) + '/' +
                getResources().getResourceTypeName(resID) + '/' +
                getResources().getResourceEntryName(resID));
    }

    public interface CountDownAnimationCallback {
        void onCountDownEnd();
    }
}