package me.timos.thuanle.base.activity;

public interface IActivityFactory {
    Class<?> getActivityClass(String tag);
}
