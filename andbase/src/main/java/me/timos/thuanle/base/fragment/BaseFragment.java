package me.timos.thuanle.base.fragment;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import me.timos.thuanle.base.event.main.MainThreadMessageEvent;
import me.timos.thuanle.base.util.ResourceHelper;

/**
 * <p>Base fragment for all fragment view in the program. You should subclass from
 * {@link BaseFragmentLayoutResId} or {@link BaseFragmentLayoutView} instead of directly from this
 * class.</p>
 * <p/>
 * <p>If you want to direct subclass from {@link BaseFragment}, make sure that neither
 * {@link #getContentResId()} or {@link #getContentView()} returns the real content view of the
 * fragment. The other must return {@code View.NO_ID} (for {@link #getContentResId()}) or
 * {@code null} (for {@link #getContentView()}).</p>
 */
public abstract class BaseFragment extends Fragment implements NavigableScreen {

    private View mRootView;
    private int xml = 0;
    private Unbinder unbinder;

    @NonNull
    public static String getRString(@StringRes int resId) {
        return ResourceHelper.getString(resId);
    }

    /**
     * Return the resId for the fragment content view. Return {@code View.NO_ID} when fragment
     * view is given by {@link #getContentView()} .
     */
    @LayoutRes
    public abstract int getContentResId();

    /**
     * Return the fragment content view. This method is only called when {@link #getContentResId()}
     * returns {@code View.NO_ID}.
     */
    public abstract View getContentView();

    /**
     * Get the root view of current fragment
     *
     * @return
     */
    protected View getRootView() {
        return mRootView;
    }

    /**
     * Init the UI after activity created.
     * @param arg
     */
    public abstract void initUI(@Nullable Bundle arg);

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    public abstract void onConfigurationChanged();

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        onConfigurationChanged();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        if (mRootView == null) {
            xml = getContentResId();
            if (xml != View.NO_ID) {
                mRootView = inflater.inflate(xml, null);
            } else {
                mRootView = getContentView();
            }
            unbinder = ButterKnife.bind(this, mRootView);
        }
        return mRootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mRootView = null;
        unbinder.unbind();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(MainThreadMessageEvent event) {/* Do something */}

    @Override
    public void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initUI(savedInstanceState);
    }


}
