package me.timos.thuanle.base.dialog;

import android.support.v4.app.DialogFragment;

public interface IDialogFactory {
    DialogFragment createDialog(String dialogTag);
}
