package me.timos.thuanle.base.fragment;

import android.support.annotation.ColorInt;
import android.support.annotation.DrawableRes;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;

public abstract class BaseBindingFragment extends BaseFragment {

    protected String edittextText(@IdRes int resId) {
        EditText et = ButterKnife.findById(getRootView(), resId);
        return et.getText().toString();
    }

    protected void gone(@IdRes int resId) {
        getRootView().findViewById(resId).setVisibility(View.GONE);
    }

    protected void goneWhen(@IdRes int resId, boolean condition) {
        View v = getRootView().findViewById(resId);
        v.setVisibility(condition ? View.GONE : View.VISIBLE);
    }

    protected void imageView(@IdRes int resId, String url) {
        ImageView iv = ButterKnife.findById(getRootView(), resId);
        Picasso.with(getContext()).load(url).into(iv);
    }

    protected void imageView(@IdRes int resId, @DrawableRes int drawableId) {
        ImageView iv = ButterKnife.findById(getRootView(), resId);
        iv.setImageResource(drawableId);
    }

    protected void invisibleWhen(@IdRes int resId, boolean condition) {
        View v = getRootView().findViewById(resId);
        v.setVisibility(condition ? View.INVISIBLE : View.VISIBLE);
    }

    protected void spinnerOnItemSelectedListener(@IdRes int resId, AdapterView.OnItemSelectedListener listener) {
        Spinner spinner = ButterKnife.findById(getRootView(), resId);
        spinner.setOnItemSelectedListener(listener);
    }

    protected int spinnerSelectedItemPosition(@IdRes int resId) {
        Spinner spinner = ButterKnife.findById(getRootView(), resId);
        return spinner.getSelectedItemPosition();
    }

    /**
     * Set text color for a specific TextView
     *
     * @param resId    the key of the TextView
     * @param colorInt the color int
     */
    protected void textColor(@IdRes int resId, @ColorInt int colorInt) {
        TextView tv = ButterKnife.findById(getRootView(), resId);
        tv.setTextColor(colorInt);
    }

    /**
     * Set a particular text to a specific textview
     *
     * @param resId the key of the TextView
     * @param text  the text
     */
    protected void textView(@IdRes int resId, CharSequence text) {
        TextView tv = ButterKnife.findById(getRootView(), resId);
        tv.setText(text);
    }

    /**
     * Set a particular text to a specific textview
     *
     * @param resId  the key of the TextView
     * @param textId the text
     */
    protected void textView(@IdRes int resId, @StringRes int textId) {
        TextView tv = ButterKnife.findById(getRootView(), resId);
        tv.setText(textId);
    }

    /**
     * Set a particular text to a specific textview
     *
     * @param resId               the key of the TextView
     * @param text                the text
     * @param visibilityWhenEmpty the visibility when text is empty.
     */
    protected void textView(@IdRes int resId, CharSequence text, int visibilityWhenEmpty) {
        TextView tv = ButterKnife.findById(getRootView(), resId);
        tv.setText(text);
        if (TextUtils.isEmpty(text)) {
            tv.setVisibility(visibilityWhenEmpty);
        }
    }

    protected void visible(@IdRes int resId) {
        getRootView().findViewById(resId).setVisibility(View.VISIBLE);
    }
}
