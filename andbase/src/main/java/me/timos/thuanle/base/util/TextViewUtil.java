package me.timos.thuanle.base.util;

import android.content.Context;
import android.os.Build;
import android.support.annotation.NonNull;
import android.text.Html;
import android.text.Spanned;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

public class TextViewUtil {
    public static void clearHint(TextView view) {
        view.setHint("");
    }

    public static void clearText(TextView... views) {
        for (final TextView view : views) {
            view.setText("");
        }
    }

    public static Spanned fromHtml(String html) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            return Html.fromHtml(html);
        } else {
            return Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT);
        }
    }

    public static void hideSoftKeyboard(Context context, View v) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static boolean isAnyEmptyText(TextView... views) {
        for (final TextView view : views) {
            if (isEmptyText(view)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isEmptyText(TextView view) {
        return TextUtils.isEmpty(view.getText());
    }

    public static boolean isNoneEmptyText(TextView... views) {
        for (final TextView view : views) {
            if (!isEmptyText(view)) {
                return true;
            }
        }
        return false;
    }

    public static String replaceAllText(TextView view, String regularExpression, String replacement) {
        return view.getText().toString().replaceAll(regularExpression, replacement);
    }

    public static String replaceText(TextView view, String target, String replacement) {
        return view.getText().toString().replace(target, replacement);
    }

    public static void showSoftKeyBoard(Context context) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    public static void showSoftKeyboard(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
    }

    @NonNull
    public static String[] splitText(TextView view, String expression) {
        return view.getText().toString().split(expression);
    }

    @NonNull
    public static String trimText(TextView view) {
        return view.getText().toString().trim();
    }
}
