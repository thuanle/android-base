package me.timos.thuanle.base.event;

public abstract class BaseDataEvent<T> extends BaseEvent {

    private final T mData;

    public BaseDataEvent(T data) {
        mData = data;
    }

    public T getData() {
        return mData;
    }
}
