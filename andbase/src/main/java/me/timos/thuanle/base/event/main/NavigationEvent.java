package me.timos.thuanle.base.event.main;

import android.os.Bundle;
import android.support.annotation.NonNull;

import java.util.ArrayList;

import me.timos.thuanle.base.event.BaseEvent;

public class NavigationEvent extends BaseEvent {
    public final Param mParam;

    private NavigationEvent(Param param) {
        mParam = param;
    }

    public static class Param {
        public String screenTag;
        public String activityTag;
        public Bundle data;
        public boolean isBackable;
        public boolean requestRefresh;
    }

    public static class Builder {
        Param mParam;

        private Builder() {
            mParam = new Param();
            mParam.data = new Bundle();
            mParam.isBackable = true;
            mParam.requestRefresh = true;
        }

        public static Builder to(@NonNull String screenTag) {
            Builder builder = new Builder();
            builder.mParam.screenTag = screenTag;

            return builder;
        }

        public static Builder to(@NonNull String activityTag, String screenTag) {
            Builder builder = new Builder();
            builder.mParam.activityTag = activityTag;
            builder.mParam.screenTag = screenTag;
            return builder;
        }

        public Builder addArg(@NonNull String key, @NonNull String value) {
            mParam.data.putString(key, value);
            return this;
        }

        public Builder addArgInt(@NonNull String key, int value) {
            mParam.data.putInt(key, value);
            return this;
        }

        public Builder addArgList(@NonNull String key, @NonNull ArrayList<String> value) {
            mParam.data.putStringArrayList(key, value);
            return this;
        }

        public NavigationEvent build() {
            return new NavigationEvent(mParam);
        }

        public Builder noBack() {
            mParam.isBackable = false;
            return this;
        }

        public Builder requestRefresh(boolean request) {
            mParam.requestRefresh = request;
            return this;
        }
    }
}
