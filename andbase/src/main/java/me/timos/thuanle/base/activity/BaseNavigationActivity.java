package me.timos.thuanle.base.activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.IdRes;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.KeyEvent;

import org.greenrobot.eventbus.Subscribe;

import me.timos.thuanle.base.R;
import me.timos.thuanle.base.application.BaseApplication;
import me.timos.thuanle.base.event.main.DialogEvent;
import me.timos.thuanle.base.event.main.NavigationEvent;
import me.timos.thuanle.base.fragment.BaseFragment;
import me.timos.thuanle.base.util.NotiUtil;
import me.timos.thuanle.base.util.ViewUtil;


/**
 * Base class for activity with navigation and toolbar tool. <br />
 * This activity can
 * <ul>
 * <li>Back button: Press twice to exit app with confirm toast.</li>
 * <li>Add fragment to backstack and pop to front if it is already loaded.</li>
 * <li>Handle Toolbar title.</li>
 * </ul>
 */
public abstract class BaseNavigationActivity extends BaseBindingActivity implements
        FragmentManager.OnBackStackChangedListener, NavigableActivity {
    public static final String EXTRA_SCREEN_TAG = "EXTRA_SCREEN_TAG";
    protected FragmentManager mFragmentManager;
    private Handler mHandler = new Handler();
    private String mCurrentFragmentTag = "";
    private boolean isExitApp = false;

    public void addFragment(@IdRes int containerViewId, String fragmentTag, Bundle data, boolean requestRefresh) {
        if (fragmentTag.equals(mCurrentFragmentTag)) {
            if (requestRefresh) {
                BaseFragment frag = (BaseFragment) mFragmentManager.findFragmentByTag(fragmentTag);
                frag.initUI(data);
            }
            return;
        }

        // check if the fragment added, pop from BackStack
        BaseFragment frag = (BaseFragment) mFragmentManager.findFragmentByTag(fragmentTag);
        if (frag != null) {
            mFragmentManager.popBackStack(fragmentTag, 0);
            if (requestRefresh) {
                frag.setArguments(data);
                frag.initUI(data);
            }
            return;
        }

        // Create new fragment
        Fragment ffrag = (Fragment) BaseApplication.getInstance().createFragment(fragmentTag);
        if (data != null) {
            ffrag.setArguments(data);
        }
        mCurrentFragmentTag = fragmentTag;
        mFragmentManager.beginTransaction()
                .add(containerViewId, ffrag, fragmentTag)
                .addToBackStack(fragmentTag)
                .commit();
    }

    protected abstract void forceCloseFocus();

    public String getCurrentFragmentTag() {
        return mCurrentFragmentTag;
    }

    @IdRes
    protected abstract int getFragmentContainerResId();

    @Override
    public void initData(Bundle savedInstanceState) {
        mFragmentManager = getSupportFragmentManager();
        mFragmentManager.addOnBackStackChangedListener(this);
    }

    @Override
    public void onBackPressed() {
        if (!isExitApp) {
            ViewUtil.hideKeyboard(this);
            NotiUtil.showToast(this, R.string.toast_prompt_exit_app);
            isExitApp = true;
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    isExitApp = false;
                }
            }, 1000);
        } else {
            System.exit(0);
        }
    }

    @Override
    public void onBackStackChanged() {
        if (mFragmentManager.getBackStackEntryCount() >= 1) {
            mCurrentFragmentTag = mFragmentManager.getBackStackEntryAt(mFragmentManager.getBackStackEntryCount() - 1).getName();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Subscribe
    public void onDialogRequest(DialogEvent event) {
        Log.d("BaseNavigationActivity", "Dialog Event: " + event.mParam.dialogTag);
        DialogFragment dialog = BaseApplication.getInstance().createDialog(event.mParam.dialogTag);
        dialog.setArguments(event.mParam.data);
        dialog.show(mFragmentManager, event.mParam.dialogTag);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            forceCloseFocus();
            popFragment();
            return true;
        } else {
            return false;
        }
    }

    @Subscribe
    public void onNavigationRequest(NavigationEvent event) {
        if (event.mParam.activityTag == null) {
            showScreen(event.mParam.screenTag, event.mParam.data, event.mParam.requestRefresh);
        } else {
            showScreen(event.mParam.screenTag, event.mParam.activityTag, event.mParam.data, event.mParam.isBackable);
        }
    }

    private void popFragment() {
        if (mFragmentManager.getBackStackEntryCount() > 1) {
            mFragmentManager.popBackStack();
        } else {
            onBackPressed();
        }
    }

    @Override
    public void showScreen(String screenTag, Bundle data) {
        showScreen(screenTag, data, false);
    }

    @Override
    public void showScreen(String screenTag, Bundle data, boolean requestRefresh) {
        addFragment(getFragmentContainerResId(), screenTag, data, requestRefresh);
    }

    @Override
    public void showScreen(String screenTag, String activityTag, Bundle data, boolean isBackable) {
        Class<?> activity = BaseApplication.getInstance().getActivityClass(activityTag);
        Intent it = new Intent(this, activity);
        it.putExtra(EXTRA_SCREEN_TAG, screenTag);
        it.putExtras(data);
        if (!isBackable) {
            finish();
        }
        startActivity(it);
    }
}
