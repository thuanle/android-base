package me.timos.thuanle.base.fragment;

public interface IFragmentFactory {
    NavigableScreen createFragment(String fragmentTag);
}
