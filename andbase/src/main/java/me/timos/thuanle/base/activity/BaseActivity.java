package me.timos.thuanle.base.activity;

import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v7.app.AppCompatActivity;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.ButterKnife;
import me.timos.thuanle.base.event.main.MainThreadMessageEvent;

public abstract class BaseActivity extends AppCompatActivity {
    protected BaseActivity getActivity() {
        return this;
    }

    @LayoutRes
    public abstract int getLayoutRedId();

    public abstract void initData(Bundle savedInstanceState);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int xml = getLayoutRedId();
        setContentView(xml);
        ButterKnife.bind(this);
        initData(savedInstanceState);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMainThreadEvent(MainThreadMessageEvent event) {/* Do something */}

    @Override
    protected void onResume() {
        super.onResume();
        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().unregister(this);
        }
    }
}
