package me.timos.thuanle.base.fragment;

import android.view.View;

/**
 * Base Fragment with layout created from a {@link View}.
 */
public abstract class BaseFragmentLayoutView extends BaseBindingFragment {
    @Deprecated
    @Override
    final public int getContentResId() {
        return View.NO_ID;
    }
}
