package me.timos.thuanle.base.util;

import android.content.Context;
import android.support.annotation.StringRes;
import android.widget.Toast;

public class NotiUtil {

    public static void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT)
                .show();
    }

    public static void showToast(Context context, @StringRes int resId) {
        Toast.makeText(context, resId, Toast.LENGTH_SHORT)
                .show();
    }
}
